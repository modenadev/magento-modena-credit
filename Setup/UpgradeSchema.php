<?php

namespace Webexpert\Credit\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

  public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    $setup->startSetup();

    if (version_compare('1.1', $context->getVersion()) == 1) {
      $this->install($setup);
    }

    $setup->endSetup();
  }

  public function install($setup) {
    $orderTable = 'sales_order_payment';

    $setup->getConnection()
      ->addColumn(
        $setup->getTable($orderTable),
        'credit_id',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          'length' => 50,
          'comment' => 'Credit Id',
        ]
      );
  }

}
