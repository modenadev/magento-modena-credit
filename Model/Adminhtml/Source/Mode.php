<?php

namespace Webexpert\Credit\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\ScopeInterface;

class Mode implements ArrayInterface {

  const MATURITYINMONTHS = '36';

  const SCOPE = 'creditpayment';

  const PATH = 'modena/api/merchant/credit-payment-order';

  public $scopeConfig;

  public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
  ) {
    $this->scopeConfig = $scopeConfig;
  }

  public function toOptionArray() {
    return [
      [
        'value' => 'test',
        'label' => 'Test',
      ],
      [
        'value' => 'production',
        'label' => 'Production',
      ],
    ];
  }

  public function getCreditUrl() {
    if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/credit/production_payment_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/credit/test_payment_url', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }

  public function getCreditRedirect() {
    if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/credit/production_redirect_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/credit/test_redirect_url', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getStoreId() {

    if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/credit/store_id', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/credit/test_store_id', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getSecret() {

    if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/credit/secret_key', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/credit/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/credit/test_secret_key', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }
}
