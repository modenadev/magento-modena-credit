<?php

namespace Webexpert\Credit\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class ConfigProvider implements ConfigProviderInterface {

  const CREDIT_CODE = 'credit';

  protected $urlInterface;

  private $storeManager;

  private $scopeConfig;

  private $checkoutSession;

  private $currencyCode;

  private $priceHelper;

  private $quoteFactory;

  protected $localeCurrency;

  public function __construct(
    \Magento\Framework\UrlInterface        $urlInterface,
    StoreManagerInterface                  $storeManager,
    ScopeConfigInterface                   $scopeConfig,
    \Magento\Checkout\Model\Session        $checkoutSession,
    CurrencyFactory                        $currencyFactory,
    \Magento\Framework\Pricing\Helper\Data $priceHelper,
    \Magento\Quote\Model\QuoteFactory      $quoteFactory,
    \Magento\Framework\Locale\CurrencyInterface $localeCurrency
  ) {
    $this->urlInterface = $urlInterface;
    $this->storeManager = $storeManager;
    $this->scopeConfig = $scopeConfig;
    $this->checkoutSession = $checkoutSession;
    $this->currencyCode = $currencyFactory->create();
    $this->priceHelper = $priceHelper;
    $this->quoteFactory = $quoteFactory;
    $this->localeCurrency = $localeCurrency;
  }

  public function getConfig() {

    return [
      'payment' => [
        self::CREDIT_CODE => [
          'redirect_url' => $this->urlInterface->getUrl('credit/request/redirect'),
          'code' => self::CREDIT_CODE,
          'display_logo' => $this->scopeConfig->getValue('payment/credit/display_logo', ScopeInterface::SCOPE_STORE),
          'logo' => $this->getLogoUrl(),
          'monthly_payment' => $this->getMonthlyPayment(),
          'oneliner'   => $this->scopeConfig->getValue('payment/credit/oneliner', ScopeInterface::SCOPE_STORE),
          'description' => $this->scopeConfig->getValue('payment/credit/description', ScopeInterface::SCOPE_STORE),
        ],
      ],
    ];
  }

  private function getLogoUrl() {
    $image = $this->scopeConfig->getValue('payment/credit/image_remote', ScopeInterface::SCOPE_STORE);

    if ($image) {
      return $image;
    }
    else {
      return FALSE;
    }
  }

  private function getMonthlyPayment() {
    $quote = $this->checkoutSession->getQuote();
    if ($quote) {
      $grandTotal = $quote->getGrandTotal();
      //customer wants this price format, not magento standard
      return round($grandTotal * '0.0325') . ' ' . $this->localeCurrency->getCurrency($this->storeManager->getStore()->getCurrentCurrencyCode())->getSymbol();
    }
    else {
      return FALSE;
    }
  }

}
