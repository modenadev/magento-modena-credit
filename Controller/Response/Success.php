<?php

namespace Webexpert\Credit\Controller\Response;

class Success extends \Magento\Framework\App\Action\Action {

  protected $logger;

  protected $request;

  protected $orderCheck;

  protected $checkoutSession;

  protected $orderFactory;

  protected $quoteFactory;

  protected $successValidator;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Checkout\Model\Session       $session,
    \Magento\Sales\Model\OrderFactory     $orderFactory,
    \Psr\Log\LoggerInterface              $logger,
    \Webexpert\Credit\Model\Request       $request,
    \Webexpert\Credit\Model\OrderCheck    $orderCheck,
    \Magento\Quote\Model\QuoteFactory     $quoteFactory,
    \Magento\Checkout\Model\Session\SuccessValidator $successValidator
  ) {
    parent::__construct($context);
    $this->logger = $logger;
    $this->request = $request;
    $this->orderCheck = $orderCheck;
    $this->checkoutSession = $session;
    $this->orderFactory = $orderFactory;
    $this->quoteFactory = $quoteFactory;
    $this->successValidator = $successValidator;
  }

  public function execute() {

    $params = filter_input_array(INPUT_POST);
    if (!$params) {
      $this->errorPage();
    }
    $order = $this->orderCheck->getOrder($params['orderId'], $params['id']);
    if (!$order) {
      $this->errorPage();
    }
    $quote = $this->quoteFactory->create()->load($order->getQuoteId());
    $this->checkBeforeSuccessPage($order, $quote);
    try {
      $status = $this->request->checkPurchase($order);
      if ($order->getState() == 'new') {
        if ($status == 'SUCCESS') {
          if ($this->orderCheck->processOrder($order)) {
            $this->messageManager->addSuccessMessage(__('Your order has been approved!'));
            $this->_redirect($this->_url->getUrl('checkout/onepage/success/', ['_secure' => TRUE]));
            return $this->_redirect('checkout/onepage/success');
          }
          else {
            $this->errorPage();
          }
        }
        elseif ($status == 'REJECTED') {
          $this->orderCheck->cancelOrder($order);
          $this->messageManager->addErrorMessage(__('Payment has been rejected.'));
          return $this->_redirect('checkout/');
        }
        elseif ($status == 'FAILED') {
          $this->orderCheck->cancelOrder($order);
          $this->messageManager->addErrorMessage(__('Could not initiate payment due to wrong module setup. Please contact the store staff.'));
          return $this->_redirect('checkout/');
        }
        else {
          $this->messageManager->addSuccessMessage(__('Your application is waiting for approval from payment provider.'));
          return $this->_redirect('checkout/onepage/success');
        }
      }
      //this happens if callback gets back first, no need to process, just show the messages
      elseif ($status == 'SUCCESS') {
        $this->messageManager->addSuccessMessage(__('Your order has been approved!'));
        $this->_redirect($this->_url->getUrl('checkout/onepage/success/', ['_secure' => TRUE]));
        return $this->_redirect('checkout/onepage/success');
      }
      elseif ($status == 'REJECTED') {
        $this->messageManager->addErrorMessage(__('Payment has been rejected.'));
        return $this->_redirect('checkout/');
      }
      elseif ($status == 'FAILED') {
        $this->messageManager->addErrorMessage(__('Could not initiate payment due to wrong module setup. Please contact the store staff.'));
        return $this->_redirect('checkout/');
      }


      else {
        return $this->_redirect('/');
      }
    } catch (\Exception $e) {
      $this->errorPage();
    }
  }

  private function errorPage() {
    $this->messageManager->addErrorMessage(__('Could not initiate payment due to wrong module setup. Please contact the store staff.'));
    return $this->_redirect('/');
  }

  public function checkBeforeSuccessPage($order, $quote) {
    if(!$this->successValidator->isValid()) {
      $this->checkoutSession
        ->setLastQuoteId($quote->getId())
        ->setLastSuccessQuoteId($quote->getId())
        ->setLastOrderId($order->getId())
        ->setLastRealOrderId($order->getIncrementId())
        ->setLastOrderStatus($order->getStatus());
    }
  }
}

