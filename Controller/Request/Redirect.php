<?php

namespace Webexpert\Credit\Controller\Request;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Redirect extends \Magento\Framework\App\Action\Action {

  protected $orderRepository;

  protected $resultJsonFactory;

  protected $checkoutSession;

  protected $logger;

  protected $request;

  protected $mode;

  protected $orderCheck;

  public function __construct(
    Context                                          $context,
    \Magento\Sales\Api\OrderRepositoryInterface      $orderRepository,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    \Magento\Checkout\Model\Session                  $checkoutSession,
    \Webexpert\Credit\Logger\Logger                  $logger,
    \Webexpert\Credit\Model\Request                  $request,
    \Webexpert\Credit\Model\Adminhtml\Source\Mode    $mode,
    \Webexpert\Credit\Model\OrderCheck               $orderCheck
  ) {
    parent::__construct($context);
    $this->orderRepository = $orderRepository;
    $this->resultJsonFactory = $resultJsonFactory;
    $this->checkoutSession = $checkoutSession;
    $this->logger = $logger;
    $this->request = $request;
    $this->mode = $mode;
    $this->orderCheck = $orderCheck;
  }

  public function execute() {

    $result = $this->resultJsonFactory->create();
    $orderId = $this->checkoutSession->getLastOrderId();
    $order = NULL;

    try {
      $order = $this->orderRepository->get($orderId);
      $modenaReturn = $this->request->initPurchase($order);
      $purchaseId = $modenaReturn['id'];
      $this->orderCheck->saveOrderPurchaseId($order->getEntityId(), $purchaseId);
      $url = $modenaReturn['location'];
    } catch (\Exception $e) {

      $this->logger->info($e->getMessage());
      return $result->setData(['error' => __($e->getMessage())]);
    }
    return $result->setData(['redirect_url' => $url]);
  }

}
