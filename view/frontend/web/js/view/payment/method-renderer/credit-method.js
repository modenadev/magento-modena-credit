/**
 * Copyright ©  Webexpert Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
define(
  [
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/place-order',
    'Webexpert_Credit/js/action/set-payment-method',
    'mage/translate',
  ],
  function ($, Component, placeOrderAction, setPaymentMethod, additionalValidators, $translate) {
    'use strict';
    return Component.extend({
      redirectAfterPlaceOrder: false,
      displayLogo: window.checkoutConfig.payment.credit.display_logo,
      defaults: {
        template: 'Webexpert_Credit/payment/credit',
      },
      getCode: function () {
        return window.checkoutConfig.payment.credit.code;
      },
      getPaymentIcon: function () {
        return window.checkoutConfig.payment.credit.logo;
      },
      getMonthlyPayment: function () {
        return window.checkoutConfig.payment.credit.monthly_payment;
      },
      getData: function () {
        return {
          'method': this.item.method,
          'additional_data': {}
        };
      },
      getOneliner: function () {
        return window.checkoutConfig.payment.credit.oneliner;
      },
      getDescription: function () {
        return window.checkoutConfig.payment.credit.description;
      },
      afterPlaceOrder: function () {
        setPaymentMethod();
      },
    });
  }
);
