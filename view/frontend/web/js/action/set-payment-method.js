define(
  [
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/model/customer',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'Magento_Checkout/js/model/full-screen-loader',
  ],
  function ($, quote, customerData, customer, globalMessageList, $t, fullScreenLoader) {
    'use strict';

    return function (messageContainer) {
      var serviceUrl,
        form;
      serviceUrl = window.checkoutConfig.payment.credit.redirect_url;
      fullScreenLoader.startLoader();
      $.ajax({
        url: serviceUrl,
        type: "get",
      }).done(function (response) {
        if (response && !response.error) {
          window.location.replace(response.redirect_url);
        } else {
          var error = {message: $t('Could not initiate payment due to wrong module setup. Please contact the store staff.')};
          messageContainer = messageContainer || globalMessageList;
          messageContainer.addErrorMessage(error);
          fullScreenLoader.stopLoader();
          return false;

        }
      }).fail(function () {
        var error = {message: $t('Could not initiate payment due to wrong module setup. Please contact the store staff.')};
        messageContainer = messageContainer || globalMessageList;
        messageContainer.addErrorMessage(error);
        fullScreenLoader.stopLoader();
        return false;
      });

    };
  }
);
