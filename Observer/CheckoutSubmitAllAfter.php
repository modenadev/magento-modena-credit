<?php

namespace Webexpert\Credit\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webexpert\Credit\Model\ConfigProvider;
use Magento\Store\Model\ScopeInterface;

class CheckoutSubmitAllAfter implements ObserverInterface {

    private $checkoutSession;
    private $scopeConfig;

    public function __construct(
        \Magento\Checkout\Model\Session\Proxy $checkoutSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
    }


  public function execute(\Magento\Framework\Event\Observer $observer) {
    $quote = $observer->getQuote();

    if ($quote->getPayment()
        ->getMethodInstance()
        ->getCode() == ConfigProvider::CREDIT_CODE &&  $this->scopeConfig->getValue('payment/credit/restore_quote', ScopeInterface::SCOPE_STORE)) {
      $quote->setReservedOrderId(NULL)
        ->setIsActive(TRUE)
        ->save();
    }

    return $this;
  }
}
