<?php

namespace Webexpert\Credit\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webexpert\Credit\Model\ConfigProvider;


class OrderEmailCancel implements ObserverInterface {

  public function execute(\Magento\Framework\Event\Observer $observer) {
    $quote = $observer->getEvent()->getQuote();
    /** @var  \Magento\Sales\Model\Order $order */
    $order = $observer->getEvent()->getOrder();

    if ($this->hasPaymentMethod($quote)) {
      $order->setCanSendNewEmailFlag(FALSE);
    }

    return $this;
  }

  protected function hasPaymentMethod($quote) {
    return $quote->getPayment()
        ->getMethodInstance()
        ->getCode() == ConfigProvider::CREDIT_CODE;
  }

}
